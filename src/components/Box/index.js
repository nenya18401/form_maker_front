import React from 'react';
import { ItemTypes } from '../../constants';
import { DragSource } from 'react-dnd';

const boxSource = {
  beginDrag(props, monitor, component) {
    const { id } = props;
    const { top, left } = component.state;
    return { id, top, left };
  },
  endDrag(props, monitor, component) {
    const dropResult = monitor.getDropResult();
    const { top, left } = dropResult;
    component.setState({
      top,
      left,
    });
  },
};

function collect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  }
}

class Box extends React.Component {
  state = {
    left: 0,
    top: 0,
  };

  render() {
    const {
      connectDragSource,
      isDragging,
      children,
      className,
    } = this.props;

    const {
      left,
      top,
    } = this.state;

    if (isDragging) {
      return null
    }

    return connectDragSource(
      <div
        className={className}
        style={{
          opacity: isDragging ? 0.5 : 1,
          backgroundColor: 'red',
          color: 'white',
          width: 150,
          height: 150,
          cursor: 'move',
          position: 'absolute',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          zIndex: 1,
          left,
          top,
        }}
      >
        {children}
      </div>
    );
  }
}

export default DragSource(ItemTypes.BOX, boxSource, collect)(Box);
