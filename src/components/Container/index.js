import React from 'react';
import { ArcherContainer, ArcherElement } from 'react-archer';
import { DropTarget } from 'react-dnd';
import { ItemTypes } from '../../constants';
import Box from '../Box';

const boxTarget = {
  drop(props, monitor, component) {
    const item = monitor.getItem();
    const delta = monitor.getDifferenceFromInitialOffset();
    const left = Math.round(item.left + delta.x);
    const top = Math.round(item.top + delta.y);
    // component.moveBox(item.id, left, top);
    return { left, top }
  },
}


function collect(connect) {
  return {
    connectDropTarget: connect.dropTarget(),
  };
}

const Container = ({connectDropTarget}) => {
  const [ box, setBox ] = React.useState(1);
  const onClickAdd = React.useCallback(() => {
    setBox(box + 1);
  }, [ box ]);
  const onClickRemove = React.useCallback(() => {
    setBox(box - 1);
  }, [ box ]);
  const boxArray = Array.from(Array(box), (x, i) => i);
  return connectDropTarget(
    <div
      className="wrapper"
      style={{
        width: '100%',
        height: '100%',
      }}
    >
      <div
        className="container"
        style={{
          backgroundColor: 'darkgrey',
          color: 'white',
          width: '100%',
          height: '100%',
          position: 'relative',
        }}
      >
        <button
          style={{
            position: 'absolute',
            right: 200,
            zIndex: 1,
          }}
          onClick={onClickAdd}
        >
          +1
        </button>
        <button
          style={{
            position: 'absolute',
            right: 200,
            top: 20,
            zIndex: 1,
          }}
          onClick={onClickRemove}
        >
          -1
        </button>
        <ArcherContainer
          strokeColor="blue"
          style={{
            height: 1000,
          }}
        >
          <Box>
            <ArcherElement
              id="asdf-0"
            >
              asdfaaaqqq
            </ArcherElement>
          </Box>
          {
            boxArray.map((b) => {
              return (
                <Box
                  key={b}
                >
                  <ArcherElement
                    id={`asdf-${b + 1}`}
                    relations={[{
                      targetId: `asdf-${b}`,
                      targetAnchor: 'right',
                      sourceAnchor: 'left',
                    }]}
                  >
                    <div>
                      asdfaaaqqq
                    </div>
                  </ArcherElement>
                </Box>
              );
            })
          }
        </ArcherContainer>
      </div>
    </div>
  );
}

export default DropTarget(ItemTypes.BOX, boxTarget, collect)(Container);

