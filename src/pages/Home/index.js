import React from 'react';
import HTML5Backend from 'react-dnd-html5-backend';
import { DragDropContextProvider } from 'react-dnd';
import Container from '../../components/Container';

const Home = () => {
  return (
  	<DragDropContextProvider backend={HTML5Backend}>
      <React.Fragment>
        <Container />
      </React.Fragment>
    </DragDropContextProvider>
  );
};

export default Home;

