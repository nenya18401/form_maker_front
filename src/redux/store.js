import {
  applyMiddleware,
  createStore,
} from 'redux';
import {
  routerMiddleware,
} from 'connected-react-router';
import {
  composeWithDevTools,
} from 'redux-devtools-extension/developmentOnly';

import history from './history';
import reducer from './reducer';

const store = createStore(
  reducer,
  composeWithDevTools({})(
    applyMiddleware(
      routerMiddleware(history),
    ),
  ),
);


export default store;
