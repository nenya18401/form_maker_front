import React from 'react';

import ReactDOM from 'react-dom';
import {
  Provider as ReduxProvider,
} from 'react-redux';
import {
  ConnectedRouter,
} from 'connected-react-router';

import store from './redux/store';
import history from './redux/history';
import Pages from './pages';
import './index.scss';

ReactDOM.render(
  <ReduxProvider store={store}>
    <ConnectedRouter history={history}>
      <Pages />
    </ConnectedRouter>
  </ReduxProvider>,
  document.getElementById('root'),
);

