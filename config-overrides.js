const getLocalIdent = require('css-loader/lib/getLocalIdent');
const {
  useEslintRc,
  addBabelPlugin,
  addDecoratorsLegacy,
  addBundleVisualizer,
  override,
} = require("customize-cra");

const replaceCssIdent = env => config => {
  if (env === 'development') {
    return config;
  } else {
    const ruleList = config.module.rules.find(x => x.oneOf);
    ruleList.oneOf.forEach((rule) => {
      const loaderList = rule.loader;
      if (Array.isArray(loaderList)) {
        if (loaderList[1].loader === require.resolve('css-loader')) {
          const cssLoader = loaderList[1];
          if (cssLoader.options.getLocalIdent) {
            cssLoader.options.getLocalIdent = getLocalIdent;
          }
        }
      }
    });
    return config;
  }
};

const replaceImageLoader = env => config => {
  const loaders = config.module.rules.find(rule => Array.isArray(rule.oneOf)).oneOf;
  const imageLoader = loaders.find(rule => {
    return rule.loader === require.resolve('url-loader');
  });
  imageLoader.options.limit = 1e3;
  return config;
};

module.exports = {
  webpack: function (config, env) {
    return override(
      useEslintRc(),
      replaceCssIdent(env),
      replaceImageLoader(env),
      addBabelPlugin('@babel/plugin-proposal-function-bind'),
      addDecoratorsLegacy(),
      process.env.BUNDLE_VISUALIZE === 'true' && addBundleVisualizer(),
    )(config);
  },
};
